Rails.application.routes.draw do
  devise_for :users
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'
  #root 'static_pages#index'
  root 'categories#index'

  resources :howtos, only: [] do
    resources :products
    resources :comments, only: [:create, :edit, :update, :destroy]
  end

  resources :categories, only: [:index] do
    # member do
    #   get :followers
    # end
    # resources :howtos, except:[:new]
    resources :howtos, except:[:new]
  end
  resources :howto_likes, only: [:create, :destroy]
  resources :howto_favorites, only: [:create, :destroy]
  
  

  resources :category_relationships, only: [:create, :destroy]
  resources :category_suggestions, only: [:index, :create, :edit, :update, :destroy]
  resources :category_suggestion_votes, only: [:create, :destroy]

  namespace :admin, except: [:show] do
    resources :categories
  end


  #resources :user_profiles, only: [:show, :new, :create, :edit, :update]
  resources :user_profiles, only: [:show, :edit, :update]
  resources :user_relationships, only: [:create, :destroy]

  get '/search_results', to: 'search_results#search'
  get '/about', to: 'static_pages#about'

  




  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
