class ProductsController < ApplicationController


  before_action :authenticate_user!, only: [:new, :create]
  before_action :require_authorized_for_current_product, only: [:new, :create, :edit, :update, :destroy] 


	def index
	  @howto = Howto.find(params[:howto_id])
	  #@products = @howto.products
	end


  def new
    @howto = Howto.find(params[:howto_id])
    @product = Product.new
    @product.photo = Photo.new

    respond_to do |format|
      format.html
      format.js
    end
  end


	def create
	  @howto = Howto.find(params[:howto_id])
    @product = @howto.products.create(product_params)

    if @product.valid?
      flash[:notice] = "Your product has been created"
      redirect_to category_howtos_path(@howto.category)
      #redirect_to new_howto_product_path(@howto), format: 'js'
    else
      #render :new, status: :unprocessable_entity
      flash[:alert] = "Sorry, something went wrong. Your product hasn't been created."
      redirect_to category_howtos_path(@howto.category)
      #redirect_to new_howto_product_path(@howto), format: :js
    end
  end


  def edit
    @howto = Howto.find(params[:howto_id])
    @product = Product.find(params[:id])

    respond_to do |format|
      format.html
      format.js
    end
  end


  def update
    @product = Product.find(params[:id])
    @product.update_attributes(product_params)

    if @product.valid?
      flash[:notice] = "Your product has been updated"
      redirect_to category_howtos_path(@product.howto.category_id)
    else
      flash[:alert] = "Sorry, something went wrong. Your product hasn't been upated."
      redirect_to category_howtos_path(@product.howto.category_id)
      #render :edit, status: :unprocessable_entity
    end
  end


  def destroy
    @product = Product.find(params[:id])
    @product.destroy
    @howto = Howto.find(params[:howto_id])
    @category = Category.find(@howto.category_id)
    redirect_to category_howtos_path(@category)
  end


	private

    def require_authorized_for_current_product
      if current_howto.user != current_user
        render text: "Unauthorized", status: :unauthorized
      end
    end

    helper_method :current_howto
    def current_howto
      @current_howto ||= Howto.find(params[:howto_id])
    end

    def product_params
      params.require(:product).permit(:name, :link, :photo_attributes => [:picture])
    end
end
