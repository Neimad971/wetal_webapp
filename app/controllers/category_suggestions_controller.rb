class CategorySuggestionsController < ApplicationController


  before_action :authenticate_user!, only: [:create, :edit, :update, :destroy]
  before_action :require_authorized_for_current_category_suggestion, only: [:edit, :update, :destroy] 


  def index
    @category_suggestions = CategorySuggestion.paginate(:page => params[:page], :per_page => 12)
  end


  def create
  	@category_suggestion = CategorySuggestion.create(category_suggestion_params.merge(user: current_user))

    if @category_suggestion.valid?
      redirect_to category_suggestions_path
    else
      flash[:alert] = "The category suggestion hasn't been created."
      #render :new, status: :unprocessable_entity #was this line before but now there is no :new action anymore
      redirect_to category_suggestions_path

    end
  end


  def edit
    @category_suggestion = CategorySuggestion.find(params[:id])

    respond_to do |format|
      format.html
      format.js
    end
  end


  def update
    @category_suggestion = CategorySuggestion.find(params[:id])
    @category_suggestion.update_attributes(category_suggestion_params)

    if @category_suggestion.valid?
      redirect_to category_suggestions_path
    else
      render :edit, status: :unprocessable_entity
    end
  end


  def destroy
    @category_suggestion = CategorySuggestion.find(params[:id])
    @category_suggestion.destroy
    redirect_to category_suggestions_path
  end


  private

    def require_authorized_for_current_category_suggestion
      if current_category_suggestion.user != current_user
        render text: "Unauthorized", status: :unauthorized
      end
    end

    helper_method :current_category_suggestion
    def current_category_suggestion
      @current_category_suggestion ||= CategorySuggestion.find(params[:id])
    end

    def category_suggestion_params
      params.require(:category_suggestion).permit(:title, :why)
    end


end
