class StaticPagesController < ApplicationController


  def index
  end


  def about
  	respond_to do |format|
      format.html
      format.js
    end
  end

  
end
