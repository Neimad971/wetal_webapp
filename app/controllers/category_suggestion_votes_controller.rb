class CategorySuggestionVotesController < ApplicationController


  before_action :authenticate_user!


  def create
    @category_suggestion = CategorySuggestion.find(params[:liked_id])
    current_user.like_category_suggestion(@category_suggestion)
    #redirect_to category_suggestions_path

    respond_to do |format|
      format.html { redirect_to category_suggestions_path }
      format.js
    end
  end


  def destroy
    @category_suggestion = CategorySuggestionVote.find(params[:id]).liked
    current_user.dislike_category_suggestion(@category_suggestion)
    #redirect_to category_suggestions_path

    respond_to do |format|
      format.html { redirect_to category_suggestions_path }
      format.js
    end
  end
end
