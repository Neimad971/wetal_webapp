class Admin::CategoriesController < ApplicationController


  before_action :authenticate_user!
  before_action :require_admin_authorization


  def index		
    # @categories = Category.all
    @categories = Category.paginate(:page => params[:page], :per_page => 8)
  end


  def new  	
  	@category = Category.new
    @category.photo = Photo.new
  end


  def create
    @category = Category.create(category_params)

    if @category.valid?
      redirect_to admin_categories_path
    else
      render :new, status: :unprocessable_entity
    end  
  end


  def edit
    @category = Category.find(params[:id])
  end


  def update
  	@category = Category.find(params[:id])
  	@category.update_attributes(category_params)
    redirect_to admin_categories_path
  end


  def destroy
  	@category = Category.find(params[:id])
  	@category.destroy
    redirect_to admin_categories_path
  end


  private

    def require_admin_authorization
      if !current_user.admin?
        return render text: 'Unauthorized', status: :unauthorized
      end
    end

    def category_params
      params.require(:category).permit(:title, :photo_attributes => [:picture])
    end

end