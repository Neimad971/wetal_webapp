class SearchResultsController < ApplicationController


  def search
  	@categories_paginator = Category.paginate(:page => params[:page], :per_page => 4)
  	@categories = @categories_paginator.search(params[:keyword])

  	@howtos_paginator = Howto.paginate(:page => params[:page], :per_page => 4)
  	@howtos = @howtos_paginator.search(params[:keyword])

  	@user_profiles_paginator = UserProfile.paginate(:page => params[:page], :per_page => 12)
  	@user_profiles = @user_profiles_paginator.search(params[:keyword])
  end


end
