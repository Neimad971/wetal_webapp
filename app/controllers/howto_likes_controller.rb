class HowtoLikesController < ApplicationController


  before_action :authenticate_user!


  def create
    @howto = Howto.find(params[:liked_id])
    current_user.like_howto(@howto)
    #redirect_to category_howtos_path(@howto.category.id)
    respond_to do |format|
      format.html { redirect_to category_howtos_path(@howto.category.id) }
      format.js
    end
  end


  def destroy
    @howto = HowtoLike.find(params[:id]).liked
    current_user.dislike_howto(@howto)
    #redirect_to category_howtos_path(@howto.category.id)
    respond_to do |format|
      format.html { redirect_to category_howtos_path(@howto.category.id) }
      format.js
    end
  end
end
