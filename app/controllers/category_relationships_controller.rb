class CategoryRelationshipsController < ApplicationController

  before_action :authenticate_user!

  def create
    @category = Category.find(params[:followed_id])
    current_user.follow(@category)
    #redirect_to categories_path
    respond_to do |format|
      format.html { redirect_to @category }
      format.js
    end
  end


  def destroy
    @category = CategoryRelationship.find(params[:id]).followed
    current_user.unfollow(@category)
    #redirect_to categories_path
    respond_to do |format|
      format.html { redirect_to @category }
      format.js
    end
  end

end
