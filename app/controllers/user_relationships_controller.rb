class UserRelationshipsController < ApplicationController


  before_action :authenticate_user!


  def create
    @user = User.find(params[:followed_id])
    current_user.follow_user(@user)
    #redirect_to user_profile_path(@user.id)
    respond_to do |format|
      format.html { redirect_to @user }
      format.js
    end
  end


  def destroy
    @user = UserRelationship.find(params[:id]).followed
    current_user.unfollow_user(@user)
    @following_count = current_user.user_following.count
    #redirect_to user_profile_path(@user.id)
    respond_to do |format|
      format.html { redirect_to @user }
      format.js
    end
  end
end
