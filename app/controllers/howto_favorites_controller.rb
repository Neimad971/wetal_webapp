class HowtoFavoritesController < ApplicationController


  before_action :authenticate_user!


  def create
    @howto = Howto.find(params[:admired_id])
    current_user.prefer_howto(@howto)
    #redirect_to category_howtos_path(@howto.category.id)
    respond_to do |format|
      format.html { redirect_to category_howtos_path(@howto.category.id) }
      format.js
    end
  end


  def destroy
    @howto = HowtoFavorite.find(params[:id]).admired
    current_user.unprefer_howto(@howto)
    #redirect_to category_howtos_path(@howto.category.id)
    respond_to do |format|
      format.html { redirect_to category_howtos_path(@howto.category.id) }
      format.js
    end
  end
end
