class UserProfilesController < ApplicationController


	before_action :authenticate_user!, only: [:edit, :update]
	before_action :require_authorized_for_current_user_profile, only: [:edit, :update]


	def show
	  @user = User.find(params[:id])
	  @user_profile = @user.user_profile

	  @favorite_howtos = @user.howto_prefering.paginate(:page => params[:page], :per_page => 4)
	  @user_howtos = @user.howtos.paginate(:page => params[:page], :per_page => 4)
	  @user_categories_following = @user.following.paginate(:page => params[:page], :per_page => 8)
	  @user_user_followers = @user.user_followers.paginate(:page => params[:page], :per_page => 12)
	  @user_user_following = @user.user_following.paginate(:page => params[:page], :per_page => 12)
	end


	def edit
      @user_profile = UserProfile.find(params[:id])
      if !@user_profile.photo.present?
      	picture = Photo.where(user_profile_id: @user_profile)
      	@user_profile.build_photo(picture: picture)
      end

      #if @user_profile.photo.present?
      #	@filename = @user_profile.photo.picture.file.filename 
      #end

      respond_to do |format|
      	format.html
      	format.js
      end
    end


    def update
      @user_profile = UserProfile.find(params[:id])
      @user_profile.update_attributes(user_profile_params)

      if @user_profile.valid?
        redirect_to user_profile_path(@user_profile.user)
      else
      	flash[:alert] = "Sorry, something went wrong. Your profile hasn't been changed."
        redirect_to user_profile_path(@user_profile.user)
      end
    end


    private

	    def require_authorized_for_current_user_profile
	      if current_user_profile.user != current_user
	        render text: "Unauthorized", status: :unauthorized
	      end
	    end

	    helper_method :current_user_profile
	    def current_user_profile
	      @current_user_profile ||= UserProfile.find(params[:id])
	    end

	    def user_profile_params
	      params.require(:user_profile).permit(:pseudo, :description, :photo_attributes => [:picture])
	    end

end
