class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception


  before_filter :get_all_categories


  
  def get_all_categories

  	# for hamburger menu and 'Publish howto' dropdown-menu 
    @sidebar_categories = Category.all

    #for pop-up howto form (new)
    @howto_form = Howto.new
    @howto_form.video = Video.new

    #for pop-up howto form (new)
    @category_suggestion_form = CategorySuggestion.new

  end
end
