class CommentsController < ApplicationController


  before_action :authenticate_user!, only: [:create]
  before_action :require_authorized_for_current_comment, only: [:edit, :update, :destroy]


  def create
	  @howto = Howto.find(params[:howto_id])
    @comment = @howto.comments.create(comment_params.merge(user: current_user))

    if @comment.valid?
      respond_to do |format|
        format.html { category_howtos_path(@howto.category_id) }
        format.js
      end
    else
      render :new, status: :unprocessable_entity # gotta change ASAP
    end
  end


  def edit
	  @howto = Howto.find(params[:howto_id])
	  @comment = Comment.find(params[:id])
  end


  def update
	  @howto = Howto.find(params[:howto_id])
	  @comment = Comment.find(params[:id])
	  @comment.update_attributes(comment_params)
	  redirect_to category_howto_path(@howto.category_id, @howto)
  end


  def destroy
	  @howto = Howto.find(params[:howto_id])
	  @comment = Comment.find(params[:id])
    @comment.destroy
	  redirect_to category_howto_path(@howto.category_id, @howto)
  end


  private

    def require_authorized_for_current_comment
      if current_comment.user != current_user
        render text: "Unauthorized", status: :unauthorized
      end
    end

    helper_method :current_comment
    def current_comment
      @current_comment ||= Comment.find(params[:id])
    end

    def comment_params
      params.require(:comment).permit(:content)
    end
end
