class HowtosController < ApplicationController


  before_action :authenticate_user!, only: [:create, :edit, :update, :destroy] #:new, 
  before_action :require_authorized_for_current_howto, only: [:edit, :update, :destroy]


  def index
    @category = Category.find(params[:category_id])
    #@howtos = @category.howtos
    @howtos = @category.howtos.paginate(:page => params[:page], :per_page => 8)
  end


  #def new
  #  @category = Category.find(params[:category_id])
  #  @howto = Howto.new
  #  @howto.video = Video.new
  #end


  def create
	  @category = Category.find(params[:category_id])
    
    @howto = @category.howtos.create(howto_params.merge(user: current_user))
    if @howto.valid?
      #redirect_to new_howto_product_path(@howto)
      flash[:notice] = "Don't forget to add products on your howto"
      redirect_to category_howtos_path(@category)
    else
      flash[:alert] = "Sorry, something went wrong. The howto hasn't been created."
      #render :new, status: :unprocessable_entity #was this line before but now there is no :new action anymore
      redirect_to category_howtos_path(@category)
    end
  end


  def show
  	@category = Category.find(params[:category_id])
  	@howto = Howto.find(params[:id])

    respond_to do |format|
      format.html
      format.js
    end
  end


  def edit
    @category = Category.find(params[:category_id])
    @howto = Howto.find(params[:id])
    #if @howto.video.present?
    #  @filename = @howto.video.video.file.filename 
    #end

    respond_to do |format|
      format.html
      format.js
    end
  end


  def update
  	@category = Category.find(params[:category_id])
  	@howto = Howto.find(params[:id])
  	@howto.update_attributes(howto_params)

    if @howto.valid?
      redirect_to category_howtos_path(@category)
    else
      flash[:alert] = "Sorry, something went wrong. The howto hasn't been updated."
      redirect_to category_howtos_path(@category)
      #render :edit, status: :unprocessable_entity
    end
  end


  def destroy
  	@category = Category.find(params[:category_id])
  	@howto = Howto.find(params[:id])
  	@howto.destroy
  	redirect_to category_howtos_path(@category)
  end


  private

    def require_authorized_for_current_howto
      if current_howto.user != current_user
        render text: "Unauthorized", status: :unauthorized
      end
    end

    helper_method :current_howto
    def current_howto
      @current_howto ||= Howto.find(params[:id])
    end

    def howto_params
      params.require(:howto).permit(:title, :description, :for_sale, :link, :yt_embedded_link,
        :video_attributes => [:video])
    end
end
