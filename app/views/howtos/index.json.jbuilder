json.array @howtos do |howto|
	json.id howto.id
	json.title howto.title
	json.video_url howto.video.url
	json.url category_howto_url(howto, category_id: howto.category.id, format: 'json')
end