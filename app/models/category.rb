class Category < ActiveRecord::Base


  validates :title, presence: true,  length: { minimum: 3 , maximum: 32}, uniqueness: true

  # has picture
  has_one :photo, :dependent => :destroy
  validates :photo, presence: true
  accepts_nested_attributes_for :photo

  # has picture many howtos
  has_many :howtos, :dependent => :destroy


  # users can follow a category
  has_many :passive_relationships, class_name:  "CategoryRelationship",
                                   foreign_key: "followed_id",
                                   dependent:   :destroy
  has_many :followers, through: :passive_relationships, source: :follower



  def self.search(keyword)
    where("lower(title) like ?", "%#{keyword.downcase}%") 
  end


end
