class Video < ActiveRecord::Base


	belongs_to :howto

	mount_uploader :video, VideoUploader

  	validates :video, presence: true,
  		file_size: { less_than_or_equal_to: 21.megabytes },
		file_content_type: { allow: ['video/quicktime', 'video/mp4', 'video/3gpp', 'video/webm'] }
		# these content types below don't work
		# 'video/x-msvideo', 'video/x-ms-wmv','video/MP2P', 'video/MP1S', 'video/x-flv', 
  	
end
