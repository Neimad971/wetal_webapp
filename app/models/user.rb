class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :confirmable


  before_create :welcome_new_user
  after_create :setup_profile
  

  has_many :category_suggestion, :dependent => :destroy
  has_many :howtos, :dependent => :destroy
  has_many :comments, :dependent => :destroy
  has_one :user_profile, :dependent => :destroy


  # follows category
  has_many :active_relationships, class_name:  "CategoryRelationship",
                                  foreign_key: "follower_id",
                                  dependent:   :destroy
  has_many :following, through: :active_relationships, source: :followed

  # likes category suggestion
  has_many :active_likes, class_name:  "CategorySuggestionVote",
                                  foreign_key: "liker_id",
                                  dependent:   :destroy
  has_many :liking, through: :active_likes, source: :liked

  # likes howto
  has_many :active_howto_likes, class_name:  "HowtoLike",
                                  foreign_key: "liker_id",
                                  dependent:   :destroy
  has_many :howto_liking, through: :active_howto_likes, source: :liked

  # has favorite howto
  has_many :active_howto_favorites, class_name:  "HowtoFavorite",
                                  foreign_key: "admirer_id",
                                  dependent:   :destroy
  has_many :howto_prefering, through: :active_howto_favorites, source: :admired

  # follows other user
  has_many :active_user_relationships, class_name:  "UserRelationship",
                                   foreign_key: "follower_id",
                                   dependent:   :destroy
  has_many :user_following, through: :active_user_relationships, source: :followed                                 

  # is followed by other user
  has_many :passive_user_relationships, class_name:  "UserRelationship",
                                    foreign_key: "followed_id",
                                    dependent:   :destroy
  has_many :user_followers, through: :passive_user_relationships, source: :follower


  def welcome_new_user
    UserMailer.confirmation_instructions(self, self.confirmation_token, {}).deliver_now
  end


  def setup_profile
    user_profile = self.create_user_profile(user_id: self.id)
  end


  # Follows a category.
  def follow(category)
    active_relationships.create(followed_id: category.id)
  end


  # Unfollows a category.
  def unfollow(category)
    active_relationships.find_by(followed_id: category.id).destroy
  end


  # Returns true if the current user is following the category.
  def following?(category)
    following.include?(category)
  end


  # Likes a category suggestion.
  def like_category_suggestion(category_suggestion)
    active_likes.create(liked_id: category_suggestion.id)
  end


  # Dislikes a category suggestion.
  def dislike_category_suggestion(category_suggestion)
    active_likes.find_by(liked_id: category_suggestion.id).destroy
  end


  # Returns true if the current user is liking the category suggestion.
  def liking_category_suggestion?(category_suggestion)
    liking.include?(category_suggestion)
  end


  # Likes a howto.
  def like_howto(howto)
    active_howto_likes.create(liked_id: howto.id)
  end


  # Dislikes a howto.
  def dislike_howto(howto)
    active_howto_likes.find_by(liked_id: howto.id).destroy
  end


  # Returns true if the current user is liking the howto.
  def liking_howto?(howto)
    howto_liking.include?(howto)
  end


  # preferes a howto.
  def prefer_howto(howto)
    active_howto_favorites.create(admired_id: howto.id)
  end


  # unprefers a howto.
  def unprefer_howto(howto)
    active_howto_favorites.find_by(admired_id: howto.id).destroy
  end


  # Returns true if the current user is prefering the howto.
  def prefering_howto?(howto)
    howto_prefering.include?(howto)
  end


  # Follows another user.
  def follow_user(other_user)
    active_user_relationships.create(followed_id: other_user.id)
  end


  # Unfollows another user.
  def unfollow_user(other_user)
    active_user_relationships.find_by(followed_id: other_user.id).destroy
  end


  # Returns true if the current user is following another user.
  def following_user?(other_user)
    user_following.include?(other_user)
  end
end
