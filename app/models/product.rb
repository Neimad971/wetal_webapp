class Product < ActiveRecord::Base

  
  validates :name, presence: true,  length: { minimum: 2 , maximum: 64 } #, uniqueness: true
  validates :link, presence: true
  validates :photo, presence: true
  

  has_one :photo, :dependent => :destroy
  accepts_nested_attributes_for :photo
  
  
  belongs_to :howto
  
end
