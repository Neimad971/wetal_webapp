class UserProfile < ActiveRecord::Base


	#validates :pseudo, presence: true,  length: { maximum: 32} #, uniqueness: true
  	#validates :description, presence: true
  	#validates :photo, presence: true


	belongs_to :user


	has_one :photo, :dependent => :destroy
    accepts_nested_attributes_for :photo


	def self.search(keyword)
      where("lower(pseudo) like ?", "%#{keyword.downcase}%") 
    end
end
