class Photo < ActiveRecord::Base


  belongs_to :category
  # or
  belongs_to :product
  # or
  belongs_to :user_profile


  mount_uploader :picture, PictureUploader
  
  
  validates :picture, presence: true, file_content_type: { allow: ['image/png', 'image/jpg', 'image/jpeg'] } 

  
end
