class HowtoFavorite < ActiveRecord::Base
	
	belongs_to :admirer, class_name: "User"
	belongs_to :admired, class_name: "Howto"
end
