class Howto < ActiveRecord::Base



  validates :title, presence: true,  length: { maximum: 100 } #, uniqueness: true
  validates :description, presence: true,  length: { maximum: 1000 } #, uniqueness: true
  #validates :video, presence: true


  belongs_to :category


  has_one :video, :dependent => :destroy
  accepts_nested_attributes_for :video


  has_many :products, :dependent => :destroy

  belongs_to :user


  has_many :passive_howto_likes, class_name:  "HowtoLike",
                                   foreign_key: "liked_id",
                                   dependent:   :destroy
  has_many :likers, through: :passive_howto_likes, source: :liker


  has_many :passive_howto_favorites, class_name:  "HowtoFavorite",
                                   foreign_key: "admired_id",
                                   dependent:   :destroy
  has_many :admirers, through: :passive_howto_favorites, source: :admirer


  has_many :comments


  def self.search(keyword)
    where("lower(title) like ?", "%#{keyword.downcase}%") 
  end


end
