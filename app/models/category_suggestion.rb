class CategorySuggestion < ActiveRecord::Base

  
  validates :title, presence: true,  length: { minimum: 2 , maximum: 32}, uniqueness: true
  validates :why, presence: true,  length: { minimum: 2 , maximum: 160} #, uniqueness: true
  

  belongs_to :user


  has_many :passive_likes, class_name:  "CategorySuggestionVote",
                                   foreign_key: "liked_id",
                                   dependent:   :destroy
  has_many :likers, through: :passive_likes, source: :liker

  
end
