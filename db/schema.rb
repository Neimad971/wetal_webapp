# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161130135002) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "categories", force: :cascade do |t|
    t.string   "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "category_relationships", force: :cascade do |t|
    t.integer  "follower_id"
    t.integer  "followed_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "category_relationships", ["followed_id"], name: "index_category_relationships_on_followed_id", using: :btree
  add_index "category_relationships", ["follower_id", "followed_id"], name: "index_category_relationships_on_follower_id_and_followed_id", unique: true, using: :btree
  add_index "category_relationships", ["follower_id"], name: "index_category_relationships_on_follower_id", using: :btree

  create_table "category_suggestion_votes", force: :cascade do |t|
    t.integer  "liker_id"
    t.integer  "liked_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "category_suggestion_votes", ["liked_id"], name: "index_category_suggestion_votes_on_liked_id", using: :btree
  add_index "category_suggestion_votes", ["liker_id", "liked_id"], name: "user_cat_sugg_index", unique: true, using: :btree
  add_index "category_suggestion_votes", ["liker_id"], name: "index_category_suggestion_votes_on_liker_id", using: :btree

  create_table "category_suggestions", force: :cascade do |t|
    t.string   "title"
    t.text     "why"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "category_suggestions", ["user_id"], name: "index_category_suggestions_on_user_id", using: :btree

  create_table "comments", force: :cascade do |t|
    t.text     "content"
    t.integer  "user_id"
    t.integer  "howto_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "comments", ["howto_id"], name: "index_comments_on_howto_id", using: :btree
  add_index "comments", ["user_id"], name: "index_comments_on_user_id", using: :btree

  create_table "howto_favorites", force: :cascade do |t|
    t.integer  "admirer_id"
    t.integer  "admired_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "howto_favorites", ["admired_id"], name: "index_howto_favorites_on_admired_id", using: :btree
  add_index "howto_favorites", ["admirer_id", "admired_id"], name: "index_howto_favorites_on_admirer_id_and_admired_id", using: :btree
  add_index "howto_favorites", ["admirer_id"], name: "index_howto_favorites_on_admirer_id", using: :btree

  create_table "howto_likes", force: :cascade do |t|
    t.integer  "liker_id"
    t.integer  "liked_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "howto_likes", ["liked_id"], name: "index_howto_likes_on_liked_id", using: :btree
  add_index "howto_likes", ["liker_id", "liked_id"], name: "index_howto_likes_on_liker_id_and_liked_id", unique: true, using: :btree
  add_index "howto_likes", ["liker_id"], name: "index_howto_likes_on_liker_id", using: :btree

  create_table "howtos", force: :cascade do |t|
    t.string   "title"
    t.integer  "category_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.text     "description"
    t.integer  "user_id"
    t.boolean  "for_sale"
    t.string   "link"
    t.string   "yt_embedded_link"
  end

  add_index "howtos", ["category_id"], name: "index_howtos_on_category_id", using: :btree

  create_table "photos", force: :cascade do |t|
    t.integer  "category_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "picture"
    t.integer  "product_id"
    t.integer  "user_profile_id"
  end

  add_index "photos", ["category_id"], name: "index_photos_on_category_id", using: :btree
  add_index "photos", ["product_id"], name: "index_photos_on_product_id", using: :btree

  create_table "products", force: :cascade do |t|
    t.string   "name"
    t.integer  "howto_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "link"
  end

  add_index "products", ["howto_id"], name: "index_products_on_howto_id", using: :btree

  create_table "user_profiles", force: :cascade do |t|
    t.integer  "user_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "pseudo"
    t.string   "description"
  end

  add_index "user_profiles", ["pseudo"], name: "index_user_profiles_on_pseudo", using: :btree
  add_index "user_profiles", ["user_id"], name: "index_user_profiles_on_user_id", using: :btree

  create_table "user_relationships", force: :cascade do |t|
    t.integer  "follower_id"
    t.integer  "followed_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "user_relationships", ["followed_id"], name: "index_user_relationships_on_followed_id", using: :btree
  add_index "user_relationships", ["follower_id", "followed_id"], name: "index_user_relationships_on_follower_id_and_followed_id", unique: true, using: :btree
  add_index "user_relationships", ["follower_id"], name: "index_user_relationships_on_follower_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.boolean  "admin",                  default: false
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "videos", force: :cascade do |t|
    t.integer  "howto_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "video"
  end

  add_index "videos", ["howto_id"], name: "index_videos_on_howto_id", using: :btree

end
