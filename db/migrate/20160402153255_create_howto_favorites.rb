class CreateHowtoFavorites < ActiveRecord::Migration
  def change
    create_table :howto_favorites do |t|

      t.integer :admirer_id
      t.integer :admired_id

      t.timestamps null: false
    end

    add_index :howto_favorites, :admirer_id
    add_index :howto_favorites, :admired_id
    add_index :howto_favorites, [:admirer_id, :admired_id], unique: true
  end
end
