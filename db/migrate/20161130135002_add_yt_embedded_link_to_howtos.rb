class AddYtEmbeddedLinkToHowtos < ActiveRecord::Migration
  def change
    add_column :howtos, :yt_embedded_link, :string
  end
end
