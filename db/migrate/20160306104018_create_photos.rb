class CreatePhotos < ActiveRecord::Migration
  def change
    create_table :photos do |t|

      t.text :caption
      t.integer :category_id
      t.timestamps null: false
    end

    add_index :photos, :category_id
  end
end
