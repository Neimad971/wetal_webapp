class CreateVideos < ActiveRecord::Migration
  def change
    create_table :videos do |t|
      t.text :caption
      t.integer :howto_id
      t.timestamps null: false
    end

    add_index :videos, :howto_id
  end
end
