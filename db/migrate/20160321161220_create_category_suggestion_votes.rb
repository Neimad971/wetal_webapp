class CreateCategorySuggestionVotes < ActiveRecord::Migration
  def change
    create_table :category_suggestion_votes do |t|
      t.integer :user_id
      t.integer :category_suggestion_id
      t.timestamps null: false
    end

    add_index :category_suggestion_votes, :user_id
    add_index :category_suggestion_votes, :category_suggestion_id
    add_index :category_suggestion_votes, [:user_id, :category_suggestion_id], unique: true, name: :user_cat_sugg_index
  end
end
