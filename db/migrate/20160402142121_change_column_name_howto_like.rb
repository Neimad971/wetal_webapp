class ChangeColumnNameHowtoLike < ActiveRecord::Migration
  def change
  	rename_column :howto_likes, :user_id, :liker_id
  	rename_column :howto_likes, :howto_id, :liked_id
  end
end
