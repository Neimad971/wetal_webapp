class AddPseudoToUserProfiles < ActiveRecord::Migration
  def change
    add_column :user_profiles, :pseudo, :string
    add_index :user_profiles, :pseudo
  end
end
