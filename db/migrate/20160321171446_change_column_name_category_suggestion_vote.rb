class ChangeColumnNameCategorySuggestionVote < ActiveRecord::Migration
  def change
  	rename_column :category_suggestion_votes, :user_id, :liker_id
  	rename_column :category_suggestion_votes, :category_suggestion_id, :liked_id
  end
end
