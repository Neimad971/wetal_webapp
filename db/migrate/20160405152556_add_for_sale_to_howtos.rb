class AddForSaleToHowtos < ActiveRecord::Migration
  def change
    add_column :howtos, :for_sale, :boolean
  end
end
