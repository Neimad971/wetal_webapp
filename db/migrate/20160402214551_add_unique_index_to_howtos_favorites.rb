class AddUniqueIndexToHowtosFavorites < ActiveRecord::Migration
  def change
  	add_index :howto_favorites, [:admirer_id, :admired_id]
  end
end
