class CreateCategorySuggestions < ActiveRecord::Migration
  def change
    create_table :category_suggestions do |t|
      t.string :title
      t.text :why
      t.integer :user_id
      t.timestamps null: false
    end
    add_index :category_suggestions, :user_id
  end
end
