class AddUserProfileIdToPhotos < ActiveRecord::Migration
  def change
    add_column :photos, :user_profile_id, :integer
  end
end
