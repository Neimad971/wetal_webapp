class RemoveUniqueIndexFromHowtosFavorites < ActiveRecord::Migration
  def change
  	remove_index :howto_favorites, column: [:admired_id, :admired_id]
  end
end
