class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
    	
      t.string :name
      t.integer :howto_id
      t.timestamps null: false
    end

    add_index :products, :howto_id
  end
end
