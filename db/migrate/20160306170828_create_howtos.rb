class CreateHowtos < ActiveRecord::Migration
  def change
    create_table :howtos do |t|

      t.string :title
      t.integer :category_id
      t.timestamps null: false
    end
 
    add_index :howtos, :category_id
  end
end
