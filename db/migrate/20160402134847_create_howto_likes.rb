class CreateHowtoLikes < ActiveRecord::Migration
  def change
    create_table :howto_likes do |t|
      t.integer :user_id
      t.integer :howto_id
      t.timestamps null: false
    end

    add_index :howto_likes, :user_id
    add_index :howto_likes, :howto_id
    add_index :howto_likes, [:user_id, :howto_id], unique: true
  end
end
