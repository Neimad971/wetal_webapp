FactoryGirl.define do
  factory :category_suggestion do
  	title "new suggested category"
  	why "I recommend this category because I'd love to see some DIY about that in order to replicate them or to buy them"
  end

end
