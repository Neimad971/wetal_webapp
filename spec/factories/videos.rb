FactoryGirl.define do
  factory :video do
    caption "makeup video"
    video Rack::Test::UploadedFile.new('spec/fixtures/files/30s.mp4', 'video/mp4')
  end

end
