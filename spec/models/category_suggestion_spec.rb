require 'rails_helper'

RSpec.describe CategorySuggestion, type: :model do
  

  it { should validate_presence_of :title }
  it { should ensure_length_of(:title).is_at_least(3) }
  it { should ensure_length_of(:title).is_at_most(32) }
  it { should validate_uniqueness_of(:title) }


  it { should validate_presence_of :why }
  it { should ensure_length_of(:why).is_at_least(20) }
  it { should ensure_length_of(:why).is_at_most(64) }
  # it { should validate_uniqueness_of(:why) }


  describe "validations" do

    before { subject.valid? }

    context "when title and why are nil" do      
      subject { FactoryGirl.build(:category_suggestion, title: nil, why: nil) }
      it "shouldn't allow blank title neither blank why" do
        expect(subject.errors_on(:title)).to include("can't be blank")
        expect(subject.errors_on(:why)).to include("can't be blank")
      end
    end

    context "when title's and why's lenght are non-acceptable (>32 characters (title) and >64 characters (why))" do
      subject { FactoryGirl.build(:category_suggestion, title: 'ab'*21, why: 'ab'*33) }
      it "shouldn't allow more than 40 characters (title) and neither more than 64 characters (why)" do
        expect(subject.errors_on(:title)).to include("is too long (maximum is 32 characters)")
        expect(subject.errors_on(:why)).to include("is too long (maximum is 64 characters)")
      end
    end


    context "when title's and why's lenght are non-acceptable (<3 characters (title) and <20 characters (why)" do
      subject { FactoryGirl.build(:category_suggestion, title: 'ab', why: 'cd') }
      it "shouldn't allow less than 20 characters (title and description)" do
        expect(subject.errors_on(:title)).to include("is too short (minimum is 3 characters)")
        expect(subject.errors_on(:why)).to include("is too short (minimum is 20 characters)")
      end
    end


    context "when title's and why's lenght are acceptable (title >=3 and <=32 chars, desc >=20 and <=64 chars)" do
      subject { FactoryGirl.build(:category_suggestion, title: 'ab'*11, why: 'cd'*11) }
      it "shouldn't have errors on title" do
        expect(subject.errors_on(:title)).to be_empty
        expect(subject.errors_on(:why)).to be_empty
      end
    end
  end

  describe "#category suggestion association" do

    before  do
      @category_suggestion = FactoryGirl.build(:category_suggestion)
      # 3.times { FactoryGirl.create :category_suggestion_vote, category_suggestion: @category_suggestion }
    end

  	it "destroys the associated likers on self destruct" do  	 
      @likers = @category_suggestion.likers
      @category_suggestion.destroy

      # likers
      @likers.each do |liker|
        expect(Category_suggestion_vote.find(liker.id)).to raise_error ActiveRecord::RecordNotFound
      end
    end
  end
end
