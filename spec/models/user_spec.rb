require 'rails_helper'

RSpec.describe User, type: :model do


  it { should validate_presence_of :email }


  describe "#user association" do

    before  do
      @user = FactoryGirl.build(:user)
      3.times { FactoryGirl.create :howto, user: @user }
    end

  	it "destroys the associated howtos on self destruct" do  	 
      
      @howtos = @user.howtos
      @following = @user.following
      @category_suggestions = @user.category_suggestion
      @liking = @user.liking
      @user.destroy

      # howtos
      @howtos.each do |howto|
        expect(Howto.find(howto.id)).to raise_error ActiveRecord::RecordNotFound
      end

      # followed category
      @following.each do |followed|
        expect(Category_relationship.find(followed.id)).to raise_error ActiveRecord::RecordNotFound
      end

      # category suggestions
      @category_suggestions.each do |category_suggestion|
        expect(Category_suggestion.find(category_suggestion.id)).to raise_error ActiveRecord::RecordNotFound
      end

      # liked suggestion
      @liking.each do |liked|
        expect(Category_suggestion_vote.find(liked.id)).to raise_error ActiveRecord::RecordNotFound
      end

    end
  end
end
