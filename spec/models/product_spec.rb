require 'rails_helper'

RSpec.describe Product, type: :model do
  

  it { should validate_presence_of :name }
  it { should ensure_length_of(:name).is_at_most(32) }

  it { should validate_presence_of :link }

  it { should validate_presence_of :photo }


  describe "validations" do

    before { subject.valid? }

    context "when name and link are nil" do      
      subject { FactoryGirl.build(:product, name: nil, link: nil) }
      it "shouldn't allow blank name neither blank link" do
        expect(subject.errors_on(:name)).to include("can't be blank")
        expect(subject.errors_on(:link)).to include("can't be blank")
      end
    end

    context "when name's lenght is non-acceptable (> 32 characters)" do
      subject { FactoryGirl.build(:product, name: 'abc'*11, link: "www.example.com") }
      it "shouldn't allow more than 32 characters for name's length" do
        expect(subject.errors_on(:name)).to include("is too long (maximum is 32 characters)")
      end
    end

    context "when name's lenght is acceptable (<=32 characters)" do
      subject { FactoryGirl.build(:product, name: 'abc'*10, link: "www.example.com") }
      it "shouldn't have errors on name" do
        expect(subject.errors_on(:name)).to be_empty
      end
    end
  end
end
