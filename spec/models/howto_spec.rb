require 'rails_helper'

RSpec.describe Howto, type: :model do

  
  it { should validate_presence_of :title }
  it { should ensure_length_of(:title).is_at_most(100) }

  it { should validate_presence_of :description } 
  it { should ensure_length_of(:description).is_at_most(1000) }

  it { should validate_presence_of :video }


  describe "validations" do

    before { subject.valid? }

    context "when title is nil" do      
      subject { FactoryGirl.build(:howto, title: nil, description: nil) }
      it "shouldn't allow blank title neither blank description" do
        expect(subject.errors_on(:title)).to include("can't be blank")
        expect(subject.errors_on(:description)).to include("can't be blank")
      end
    end

    context "when title's and description's lenght are non-acceptable (>100 characters (title) and >1000 characters (desc))" do
      subject { FactoryGirl.build(:howto, title: 'ab'*51, description: 'ab'*501) }
      it "shouldn't allow more than 100 characters (title) and neither more than 1000 characters (desc)" do
        expect(subject.errors_on(:title)).to include("is too long (maximum is 100 characters)")
        expect(subject.errors_on(:description)).to include("is too long (maximum is 1000 characters)")
      end
    end

    context "when title's and description's lenght are acceptable (title <=100 chars, desc <=1000 chars)" do
      subject { FactoryGirl.build(:howto, title: 'ab'*41, description: 'cd'*401) }
      it "shouldn't have errors on title" do
        expect(subject.errors_on(:title)).to be_empty
        expect(subject.errors_on(:description)).to be_empty
      end
    end
  end


  describe "#howto association" do

    before  do
      @howto = FactoryGirl.build(:howto)
      3.times { FactoryGirl.create :product, howto: @howto }
    end

  	it "destroys the associated products on self destruct" do  	 
      @products = @howto.products
      @howto.destroy

      # products
      @products.each do |product|
        expect(Product.find(product.id)).to raise_error ActiveRecord::RecordNotFound
      end
    end
  end
end
