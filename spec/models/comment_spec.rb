require 'rails_helper'

RSpec.describe Comment, type: :model do
  

  it { should validate_presence_of :content }


  describe "validations" do

    before { subject.valid? }

    context "when content is nil" do      
      subject { FactoryGirl.build(:comment, content: nil) }
      it "shouldn't allow blank content" do
        expect(subject.errors_on(:content)).to include("can't be blank")
      end
    end


    context "when content's acceptable" do
      subject { FactoryGirl.build(:comment, content: 'ab'*41) }
      it "shouldn't have errors on comment" do
        expect(subject.errors_on(:content)).to be_empty
      end
    end
  end
end
