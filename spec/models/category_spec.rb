require 'rails_helper'

RSpec.describe Category, type: :model do


  it { should validate_presence_of :title }
  it { should ensure_length_of(:title).is_at_least(3) }
  it { should ensure_length_of(:title).is_at_most(32) }
  it { should validate_uniqueness_of(:title) }

  it { should validate_presence_of :photo }
  

  describe "validations" do
    before { subject.valid? }

    context "when title is nil" do
      subject { Category.new(:title => nil) } 
      it "shouldn't allow blank title" do
        expect(subject.errors_on(:title)).to include("can't be blank")
      end
    end

    context "when title's lenght is non-acceptable (> 32 characters)" do
      subject { Category.new(:title => 'abc'*11) }
      it "shouldn't allow more than 32 characters for title's length" do
        expect(subject.errors_on(:title)).to include("is too long (maximum is 32 characters)")
      end
    end

    context "when title's lenght is non-acceptable (< 3 characters)" do
      subject { Category.new(:title => 'a') }
      it "shouldn't allow less than 2 characters for title's length" do
        expect(subject.errors_on(:title)).to include("is too short (minimum is 3 characters)")
      end
    end

    context "when title's lenght is acceptable (between 3 and 32 characters)" do
      subject { Category.new(:title => 'abc'*10) }
      it "shouldn't have errors on title" do
        expect(subject.errors_on(:title)).to be_empty
      end
    end
  end



  describe "#category association" do

    before  do
      @category = FactoryGirl.build(:category)
      3.times { FactoryGirl.create :howto, category: @category }
      # 3.times { FactoryGirl.create :category_relationship, category: @category }
    end

  	it "destroys the associated photo, howtos and passive relationships (followers) on self destruct" do  	 
      @photo = @category.photo
      @howtos = @category.howtos
      @followers = @category.followers
      @category.destroy

      # photo
      expect { Photo.find(@photo.id) }.to raise_error ActiveRecord::RecordNotFound

      # howtos
      @howtos.each do |howto|
        expect(Howto.find(howto.id)).to raise_error ActiveRecord::RecordNotFound
      end

      # followers
      @followers.each do |follower|
        expect(Category_relationship.find(follower.id)).to raise_error ActiveRecord::RecordNotFound
      end
    end

  end

end
